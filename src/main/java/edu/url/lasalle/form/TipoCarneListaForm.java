package edu.url.lasalle.form;

import java.util.List;
import edu.url.lasalle.model.TipoCarne;

public class TipoCarneListaForm {
	
	private List<TipoCarne> tiposCarne;
	
	public List<TipoCarne> getTiposCarne() { 
		return tiposCarne;
	}
	
	public void setTiposCarne(List<TipoCarne> tiposCarne) { 
		this.tiposCarne = tiposCarne;
	}
}
