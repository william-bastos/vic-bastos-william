package edu.url.lasalle.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = {"/"})
public class DefaultController {
	
	@RequestMapping()
	public ModelAndView inici() {
		
		//int a = 5/0;
		
		return new ModelAndView("inicio");
		
	}
}
