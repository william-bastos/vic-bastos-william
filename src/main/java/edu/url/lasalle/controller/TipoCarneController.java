package edu.url.lasalle.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import edu.url.lasalle.form.TipoCarneListaForm;
import edu.url.lasalle.model.TipoCarne;
import edu.url.lasalle.service.TipoCarneService;

@Controller
@RequestMapping(value = {"/tipocarne"})
public class TipoCarneController {
	
	@Autowired
	private TipoCarneService tipoCarneService;
	
	private static final Logger log = LoggerFactory.getLogger(TipoCarneController.class);
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView listarTiposCarne() {
		
		List<TipoCarne> listarTiposCarne = tipoCarneService.listarTiposCarne();
		
		TipoCarneListaForm tipoCarneListaForm = new TipoCarneListaForm();
		
		tipoCarneListaForm.setTiposCarne(listarTiposCarne);
		
		if(!listarTiposCarne.isEmpty()) {
			log.info(listarTiposCarne.get(0).getNombre());
		}
		
		return new ModelAndView("tipoCarneLista","tipoCarneListaForm", tipoCarneListaForm);
		
	}
}
