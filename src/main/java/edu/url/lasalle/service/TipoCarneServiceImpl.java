package edu.url.lasalle.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.url.lasalle.model.TipoCarne;
import edu.url.lasalle.repository.TipoCarneRepository;

@Service
public class TipoCarneServiceImpl implements TipoCarneService {

	@Autowired
	TipoCarneRepository tipoCarneRepository;

	public List<TipoCarne> listarTiposCarne() {
		List<TipoCarne> listaTipoCarne = tipoCarneRepository.findAll();
		return (listaTipoCarne);
	}
}
