package edu.url.lasalle.service;

import java.util.List;

import edu.url.lasalle.model.TipoCarne;

public interface TipoCarneService {
	
	public List<TipoCarne> listarTiposCarne();
	
}
