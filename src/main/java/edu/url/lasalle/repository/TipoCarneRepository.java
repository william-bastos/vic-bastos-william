package edu.url.lasalle.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import edu.url.lasalle.model.TipoCarne;

@Repository
public interface TipoCarneRepository extends JpaRepository< TipoCarne, Integer>{

}
